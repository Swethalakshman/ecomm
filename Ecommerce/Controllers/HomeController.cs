﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ecommerce.Models;

namespace Ecommerce.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Business Bo;
        public static string pno;
        public HomeController(ILogger<HomeController> logger, Business Bo)
        {
            _logger = logger;
            this.Bo = Bo;
        }

        public IActionResult Index()
        {
            var rec = Bo.GetProducts();
            var catlist = Bo.GetCategories();
            ViewBag.CategoryList = catlist;
            return View(rec);
        }
        [HttpPost]
        public IActionResult SaveUserByMobileNumber(string MobileNumber)
        {
           pno = MobileNumber
           if((pno.Contains(" ")) || (pno.Contains("-")))
           {
               pno = pno.Replace(" ","");
               pno = pno.Replace("-","");
           }
           var success = -1;
            try
            {
                if (string.IsNullOrEmpty(MobileNumber))
                {
                    return Json("Invalid Mobile Number");
                }
                success = Bo.SaveUserByMobileNumber(MobileNumber);
            }
            catch(Exception e)
            {
                return Json(e.Message);
            }
            
            return Json(success);
        }
        [HttpPost]
        public IActionResult Addtocart(string outs)
        {
           var success = -1;
            try
            {
                if (string.IsNullOrEmpty(outs))
                {
                    return Json("Invalid");
                }
                success = Bo.addToCart(outs,pno);
                return Json(success);
            }
            catch(Exception e)
            {
                return Json(e.Message);
            }
            
        }
        [HttpPost]
        public IActionResult Addtocart(string outs)
        {
           var upd = -1;
            try
            {
                upd=Bo.UpdateStatus(pno)
                return Json(upd)
            }
                        catch(Exception e)
            {
                return Json(e.Message);
            }           
         }  
      
    }
}
