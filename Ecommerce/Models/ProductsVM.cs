﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class ProductsVM
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string CatagoryName { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Price { get; set; }
        public string ImgPath { get; set; }
       
    }
}
