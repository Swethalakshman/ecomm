﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class UnitOfWork
    {
        private readonly DbContext _dbContext;
        private Dictionary<string, dynamic> _repositories;
        private readonly IHttpContextAccessor _httpContext;
        public UnitOfWork(DbContext dbContext, IHttpContextAccessor context)
        {
            this._dbContext = dbContext;
            _httpContext = context;
        }
        public void BeginTransaction()
        {
            try
            {
                _dbContext.Database.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Commit()
        {
            try
            {
                _dbContext.Database.CommitTransaction();
            }
            catch (Exception ex)
            {
                Rollback();
                throw ex;
            }
        }
        public string GetPKName(object entity)
        {
            string key = string.Empty;
            try
            {
                key = _dbContext.Model.FindEntityType(entity.GetType()).FindPrimaryKey()
                    .Properties.Select(x => x.Name).Single();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return key;
        }

        public int GetPKValue(object entity)
        {
            int keyVal;
            try
            {
                string key = GetPKName(entity);
                keyVal = (int)entity.GetType().GetProperty(key).GetValue(entity, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return keyVal;
        }
        public IRepository<Entity> Repository<Entity>() where Entity : class
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<string, dynamic>();
            }

            var type = typeof(Entity).Name;

            if (_repositories.ContainsKey(type))
            {
                return (IRepository<Entity>)_repositories[type];
            }

            var repositoryType = typeof(Repository<>);

            _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(Entity)), _dbContext, _httpContext));

            return _repositories[type];
        }
        public void Rollback()
        {
            try
            {

                _dbContext.Database.RollbackTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SaveChanges()
        {
            int IsSaved;
            try
            {
                if (_dbContext.ChangeTracker.QueryTrackingBehavior != QueryTrackingBehavior.NoTracking)
                {
                    AddAuditfldValues();
                }
                _dbContext.SaveChanges();
                IsSaved = 1;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                ex.Entries.Single().Reload();
                var entityVal = ex.Entries.Single().GetDatabaseValues();
                if (entityVal == null)
                {
                    throw ex;
                }
                else
                {
                    ex.Entries.Single().CurrentValues.SetValues(entityVal);
                    ex.Entries.Single().OriginalValues.SetValues(entityVal);
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return (IsSaved);
        }

        private void AddAuditfldValues()
        {
            DateTime now = DateTime.Now;
            var userName = _httpContext.HttpContext.User.Claims.Where(e => e.Type == "UserName").Select(e => e.Value).FirstOrDefault();
            if (userName == null)
                userName = "System";
            foreach (EntityEntry<AuditableBase> entry in _dbContext.ChangeTracker.Entries<AuditableBase>())
            {
                var obj = entry.Entity;
                if (entry.State == EntityState.Added)
                {
                    obj.GetType().GetProperty("CreatedBy").SetValue(obj, userName);
                    obj.GetType().GetProperty("CreatedDate").SetValue(obj, now);
                    obj.GetType().GetProperty("UpdatedBy").SetValue(obj, userName);
                    obj.GetType().GetProperty("UpdatedDate").SetValue(obj, now);
                    obj.GetType().GetProperty("IsActive").SetValue(obj, true);

                }
                else if (entry.State == EntityState.Modified)
                {
                    obj.GetType().GetProperty("UpdatedBy").SetValue(obj, userName);
                    obj.GetType().GetProperty("UpdatedDate").SetValue(obj, now);
                    entry.Property("CreatedBy").IsModified = false;
                    entry.Property("CreatedDate").IsModified = false;
                }
            }
        }

    }
}
